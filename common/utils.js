angular.module('app')

    .factory('utils', function() {
        return {
            //returns value depending on language
            getLocalizedValue: function(info, lang) {
                var result = info.filter(function(val) {
                    return val.lang === lang;
                });

                return result[0].value;
            }
        };
    });