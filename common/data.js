angular.module('app').factory('data', function () {
    //data store for current location Id
    var data = {
        currentLocationId: ''
    };

    return {
        getCurrentLocationId: function () {
            return data.currentLocationId;
        },
        setCurrentLocationId: function (id) {
            data.currentLocationId = id;
        }
    };
});