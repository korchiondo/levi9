angular.module('eventsList')

.factory('eventsService', ['$http', 'utils', function($http, utils) {

    var lang = "en-GB";

    //modify data
    function prepareData(data) {
        return data && data.event && data.event.map(function(val) {
            return {
                id: val.id,
                locatorId: val.location.relationship[0].targetPOI,
                name: utils.getLocalizedValue(val.label, lang)
            }
        });
    }

    //get all events
    function getEvents() {
        return $http
            .get('http://citysdk.dmci.hva.nl/CitySDK/events/search?category=festival')
            .then(function(info) {
                return prepareData(info.data);
            });
    }

    return {
        getEvents: getEvents
    };
}]);
