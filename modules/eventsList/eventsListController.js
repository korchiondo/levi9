angular.module('eventsList')
 
.controller('EventsListController',
    [ '$scope', 'eventsService', 'data',
    function ($scope, eventsService, data) {

        var vm = this;

        //get all events and set controller properties
        function init() {
            eventsService
                .getEvents()
                .then(function(events) {
                    vm.events = events;
                    vm.currentEventId = vm.events[0].id;
                    vm.currentLocatorId = vm.events[0].locatorId;
                });
        }

        //handle onclick by setting controller properties
        vm.onEventClick = function(id) {
            var events = vm.events.filter(function (val) {
                    return val.id == id;
                }) || vm.events;
            vm.currentEventId = events[0].id;
            vm.currentLocatorId = events[0].locatorId;
        };

        //watch if location change set new location id to data service
        $scope.$watch(angular.bind(this, function () {
            return this.currentLocatorId;
        }), function (newValue, oldValue) {
            if (newValue !== oldValue) {
                data.setCurrentLocationId(newValue);
            }
        });

        init();
}]);