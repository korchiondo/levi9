//create controller for map with required dependencies
angular.module('map')

.controller('MapController', ['$scope', 'mapService', 'data', function ($scope, mapService, data) {

    var vm = this;

    //get map model properties to controller
    function init() {
        angular.extend(vm, mapService.getMapModel());
    }

    //watch location id in data service and if it's changed - get new location data from server and set new
    // map properties to controller
    $scope.$watch(function () {
        return data.getCurrentLocationId();
    }, function (newValue, oldValue) {
        if (newValue !== oldValue) {
            mapService
                .getEventById(newValue)
                .then(function(mapModel) {
                    angular.extend(vm, mapModel);
                });
        }
    });

    init();

}]);