angular.module('map')

.factory('mapService', ['$http', '$sce', 'utils', function($http, $sce, utils) {

    var lang = "en-GB",
        mapModel = {
            map: {
                center: {
                    latitude: 0,
                    longitude: 0
                },
                zoom: 16
            },
            marker: {
                coords: {
                    latitude: 0,
                    longitude: 0
                },
                id: 0
            },
            description: ''
        };

        //get event by id, return map model
        function getEventById(id) {
            return $http
                .get('http://citysdk.dmci.hva.nl/CitySDK/pois/' + id)

                .then(function(event) {
                    return getMapModel(event.data);
                });
        }

        //get map model
        function getMapModel(event) {
            if(event && event.location) {
                var coordinates = event.location.point[0].Point.posList.split(" ");
                mapModel.map.center.latitude = coordinates[0] ;
                mapModel.map.center.longitude = coordinates[1];
                mapModel.marker.coords.latitude = coordinates[0];
                mapModel.marker.coords.longitude = coordinates[1];
                mapModel.marker.id = event.id;
                mapModel.description = $sce.trustAsHtml(utils.getLocalizedValue(event.description, lang)) || "";
            }
            return mapModel;
        }


    return {
        getEventById: getEventById,
        getMapModel: getMapModel
    };
}]);