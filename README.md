Test task - Festivals in Amsterdam
=============================

DESCRIPTION
-----------

Frameworks and libraries: AngularJS, Bootstrap.

Angular google maps directive was used to integrate google map into application.

Application  consist of: main module application - app.js; two modules which represents list part and map part of the page; common folder include some utiles function and data storage which required for communication between modules.

REQUIREMENTS
------------

Firefox is required for local launching (without web-server). For Chrome launching it's necessary to install web server.

INSTALLATION
------------

No installation is required. But if it's necessary to launch application in Chrome you can install web server Express

    $ npm install

(of course you will need node and npm for this)

QUICK START
-----------

Just launch index.html.

For Chrome browser you should run web-server:

    $ npm start

After that you can access web page by typing localhost:8080 in your web browser