//create application and put necessary dependency
angular.module('eventsList', []);
angular.module('map', []);

angular.module('app', [
    'eventsList',
    'map',
    'uiGmapgoogle-maps']).run();